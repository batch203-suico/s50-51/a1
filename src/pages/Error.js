import Banner from '../components/Banner';

export default function Error() {

	// We created an object variable named "data" to contains the properties we want to sent in the Banner component.
	const data = {
		title: "404 - Not found",
		content: "The page you are looking for cannot be found",
		destination: "/",
		label: "Back home"
	}

	return (
		<Banner data={data}/>
	)
}